# Start with a suitable docker container that has node and npm installed
# The as base is so that we can refer to it in a second stage - https://docs.docker.com/develop/develop-images/multistage-build/
FROM node:alpine as base
# set the default directory that commands are run from to /home/node - why there? I went into the container and saw that folder existed in the home dir
WORKDIR /home/node
# install pnpm package manager
RUN npm install -g pnpm

# now I can reference the above as 'base' which has extended the original node:alpine with pnpm
FROM base as build
# copy my current work directory with all our source code into the /home/node director
COPY . /home/node
# install our dependencies
RUN pnpm install
# build our code
RUN pnpm build

# Final docker container starting from base again
FROM base
# only copy the minimum required, our built code, our node_modules and the package.json...just to make use of the start script
COPY --from=build /home/node/dist /home/node/dist
COPY --from=build /home/node/node_modules /home/node/node_modules
COPY --from=build /home/node/package.json /home/node/package.json
# expose port 80 so that when its running we can hit the API
EXPOSE 80
# default run command when starting this container
CMD pnpm start

