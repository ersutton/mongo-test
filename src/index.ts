import { createConnection } from 'mongoose'
import { Express } from 'express'
import * as express from 'express'
import * as bp from 'body-parser'
import { UserReadRepository, UserWriteRepository } from './mongo'


const config = {
	port: process.env.PORT || 80,
	mongoRead: process.env.MONGO_READ || 'mongodb://localhost:27017/example',
	mongoWrite: process.env.MONGO_WRITE || 'mongodb://localhost:27017/example',
}

main()
.then(_ => {
	// Setup listeners to close app gracefully
})
.catch(err => console.log(err))

async function main() {
	console.log('application start')
	
	// create two connections, one for readonly operations...
	const mongoReadConnection = createConnection(config.mongoRead)
	/**
	 * One for write operations. We make this separation early - even though the connection string is the same.
	 * We make this separation early so that later on we add read replicas to our mongo database and update the read
	 * replica string. Then all our read operations are correctly diverted away from the master.
	 */
  	const mongoWriteConnection = createConnection(config.mongoWrite)

	// Create read and write repos
	const writeRepository = new UserWriteRepository(mongoWriteConnection)
	const userReadRepository = new UserReadRepository(mongoReadConnection)

	// create simple express server
	const app: Express = express()
	app.use(bp.json())
	app.use(bp.urlencoded({extended: true}))

	app.get('/', (_, res) => {
		res.send('Hello world')
	})

	app.get('/users', async (_, res) => {
		const users = await userReadRepository.getAllPlain()
		res.send(JSON.stringify(users))
	})

	app.post('/user', async (req, res) => {
		console.error(req.body)
		await writeRepository.create(req.body)
		res.sendStatus(200)
	})

	app.listen(config.port, () => {
		console.log(`app listening on port ${config.port}`)
	})
}

