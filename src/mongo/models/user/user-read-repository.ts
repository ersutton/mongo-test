import { Connection, Document } from 'mongoose'

import { userSchema, User, UserModel, USER_COLLECTION } from './user'

/**
 * Write models in read and write repositories (optional, you can be lazy and just create UserRepository that takes the master mongo node)
 * By creating this separation, even if under the hood you supply the same master connection, later on, when you want to scale, you can
 * add mongo read replicas, update the connection string to connect to the replica and all your read queries are now correctly directed at replicas and not the master
 */
export class UserReadRepository {
	private readonly connection: Connection

	constructor(connection: Connection) {
		this.connection = connection
		this.connection.model<User>(USER_COLLECTION, userSchema)
	}
	private get user(): UserModel {
		return this.connection.model<User>(USER_COLLECTION)
	}

	/** finds a user by the supplied name */
	async findByName(name: string) {
		return this.user.findOne({name})
	}

	/**
	 * gets all users in the database
	 */
	async getAll() {
		return this.user.find()
	}

	async getAllPlain() {
		const users = await this.getAll()
		return users.map(x => x.toObject())
	}
}
