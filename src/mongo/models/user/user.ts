import { Schema, Model } from 'mongoose';

// Name of the collection in the db
export const USER_COLLECTION = 'User'
// 1. Create an interface representing a document in MongoDB.
export interface User {
  name: string;
  email: string;
  avatar?: string;
}

// 2. Create a Schema corresponding to the document interface.
export const userSchema = new Schema<User>({
  name: { type: String, required: true },
  email: { type: String, required: true },
  avatar: String
});

// 3. Create a Model.
export type UserModel = Model<User>
