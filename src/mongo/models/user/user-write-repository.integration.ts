import { Connection, createConnection } from 'mongoose'
import { UserWriteRepository } from './user-write-repository'

import { User, USER_COLLECTION } from './user'
import * as faker from 'faker'

describe('UserWriteRepository', () => {
	let sut: UserWriteRepository
	let masterConnection: Connection
	beforeAll(async () => {
		// Be sure to have run docker-compose up prior to this
		// first create a connection
		masterConnection = createConnection(process.env['MONGO_WRITE']!)

		sut = new UserWriteRepository(masterConnection)
	})
	afterAll(async () => {
		try {
			// drop previous Users
			await masterConnection.db.dropCollection('users')
			
		} catch (e) {
			console.error(e)
		} finally {
			await masterConnection.close()
		}
	})

	describe('create', () => {
		it('should be able to create a new user', async () => {
			const user: User = {
				name: faker.name.firstName(),
				email: faker.internet.email()
			}
			await sut.create(user)

			// check it can be grabbed out of the db
			const userPulledFromDb = await masterConnection.model(USER_COLLECTION).findOne({name: user.name})
			expect(userPulledFromDb.toObject()).toMatchObject(user)
		})
	})
})