import { Connection } from 'mongoose'

import { userSchema, User, UserModel, USER_COLLECTION } from './user'

/**
 * Write models in read and write repositories (optional, you can be lazy and just create UserRepository that takes the master mongo node)
 * By creating this separation, even if under the hood you supply the same master connection, later on, when you want to scale, you can
 * add mongo read replicas, update the connection string to connect to the replica and all your read queries are now correctly directed at replicas and not the master
 */
export class UserWriteRepository {
	private readonly connection: Connection

	constructor(connection: Connection) {
		this.connection = connection
		this.connection.model<User>(USER_COLLECTION, userSchema)
	}
	private get user(): UserModel {
		return this.connection.model<User>(USER_COLLECTION)
	}

	/**
	 * create a user
	 */
	async create(user: User) {
		return new this.user(user).save()
	}
}
