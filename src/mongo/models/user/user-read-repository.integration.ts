import { Connection, createConnection, Model } from 'mongoose'
import { UserWriteRepository } from './user-write-repository'

import { userSchema, User, UserModel, USER_COLLECTION } from './user'
import * as faker from 'faker'
import { UserReadRepository } from './user-read-repository'

describe('UserReadRepository', () => {
	let sut: UserReadRepository
	let masterConnection: Connection
	let readConnection: Connection
	beforeAll(async () => {
		// Be sure to have run docker-compose up prior to this
		// first create a connection
		masterConnection = createConnection(process.env['MONGO_WRITE']!)
		readConnection = createConnection(process.env['MONGO_READ']!)

		// setup the model on the connection - this wont be required in other tests that consume our read/write repos
		// as this is handled in their constructor
		masterConnection.model<User>(USER_COLLECTION, userSchema)
		
		sut = new UserReadRepository(readConnection)
	})
	afterAll(async () => {
		await masterConnection.close()
		await readConnection.close()
	})

	afterEach(async () => {
		await masterConnection.db.dropCollection('users')
	})

	describe('findByName', () => {
		it('should be able to find a user by its name', async () => {
			const user: User = {
				name: faker.name.firstName(),
				email: faker.internet.email()
			}
			await new (masterConnection.model<User>(USER_COLLECTION) as UserModel)(user).save()
			// check it can be grabbed out of the db
			const userPulledFromDb = await sut.findByName(user.name)

			expect(userPulledFromDb.toObject()).toMatchObject(user)
		})
	})

	describe('getAll', () => {
		it('should be able to return all users', async () => {
			const user: User = {
				name: faker.name.firstName(),
				email: faker.internet.email()
			}
			const secondUser: User = {
				name: faker.name.firstName(),
				email: faker.internet.email()
			}
			await new (masterConnection.model<User>(USER_COLLECTION) as UserModel)(user).save()
			await new (masterConnection.model<User>(USER_COLLECTION) as UserModel)(secondUser).save()
			// check it can be grabbed out of the db
			const usersPulledFromDb = await sut.getAll()
			const pojoUsers = usersPulledFromDb.map(u => u.toObject())
			expect(pojoUsers).toHaveLength(2)
			expect(pojoUsers.find(x => x.name === user.name)).toMatchObject(user)
			expect(pojoUsers.find(x => x.name === secondUser.name)).toMatchObject(secondUser)
		})
	})

	describe('getAllPlain', () => {
		it('should be able to return all users in a pojo format', async () => {
			const user: User = {
				name: faker.name.firstName(),
				email: faker.internet.email()
			}
			const secondUser: User = {
				name: faker.name.firstName(),
				email: faker.internet.email()
			}
			await new (masterConnection.model<User>(USER_COLLECTION) as UserModel)(user).save()
			await new (masterConnection.model<User>(USER_COLLECTION) as UserModel)(secondUser).save()
			// check it can be grabbed out of the db
			const pojoUsers = await sut.getAll()
			expect(pojoUsers).toHaveLength(2)
			expect(pojoUsers.find(x => x.name === user.name)).toMatchObject(user)
			expect(pojoUsers.find(x => x.name === secondUser.name)).toMatchObject(secondUser)
		})
	})
})