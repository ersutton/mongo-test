# mongo-test

Example mongo playground written in Typescript, sorry I can't write javascript I lose my mind. Hopefully you can just manually remove the types and see through to the underlying JS to see how it works.

## Prerequisites
1. node
2. docker & docker-compose

## Getting started
1. start the environment by:
```bash
cd docker
docker-compose up
```

This will download and run a mongo db for you, and bind port 27017 to your host.

2. run: `pnpm i` to install dependencies - I've setup using `pnpm` as that's what I'm used to - feel free to use npm or yarn instead
3. run tests `pnpm test:watch <file>` to run whichever test you want - there are two integration tests for the read and write repository for mongo I've created
4. run the application which is started in index.ts using `pnpm dev` - or you can build and run using node by `pnpm build && pnpm start` all scripts that I'm running here are in `package.json`

## Notes
`pnpm build` seems to fail on some weird mongo issues - you wont have these teething issues in js, I'm more comfortable in TS - sorry about this.


## Test structure
* Configured using `jest.json` in root
* use a tool called `dotenv-cli` for passing in the test.env environment file for configuration, there's a local.env for standard running. This will use two different databases, one for you local development, one simply for tests that can be destroyed at will


## Docker
### Use cases
* Trying software quickly without having to install directly to your system. Example, if you needed to connect to postgres server to pull a file, you could simply `docker run --rm -it postgres bash` and go from there to connect to a server, query it, pull down a file etc.
* Setting up repeatable dev environments. Example if you know your app needs a database, redis for queues and caching, simply create a `docker-compose` file with these and you have the stateful part of your app ready to go. Your node app can simply connect and use that - with the intention of it becoming a docker container once development has completed...
* Deployment - one of the largest advantages to docker is being able to package your app and deploy your code to pretty much any cloud environment and take advantage of:
	* self healing containers - most docker orchestrators (docker-swarm, k8s, aws ECS) can have the docker configuration setup to add health checks to your containers endpoints, and have them recreate containers that fail these - they can also tell when your app exits unexpectently and start another
	* scaling - you can setup feedback loops based on metrics in docker orchestrators - queue depth, API response times or if desperate CPU/RAM usage of your containers. If the queue your service worker is getting too large, the orchestrator can scale up and now you have 2 docker containers helping reduce the queue to 0. You can state how much ram each container needs to run - and if the AWS instance isn't large enough to handle it, it can scale up another AWS instance to run docker on to spin the container up on. In this way, you can have your infrastructure run on cheap spot instance resources that scale up when needed, creating new instances, and when the queue is under control scale down the workers, and then in turn scale down the un needed instances to save costs
	* Cheap compute - in the above example, we scaled out an in a worker - if the worker is servicing messages off a queue, you can use cheap spot instances on infrastructure - these are cheap instances that can be taken off you with a 5 minute warning. There are tools such as spot.io that help you scale out and in your k8s clusters with spot instances

	

